import random
import os
from tkinter import *
red = []
blue = []

os.system('clear')

with open('questionsr.txt', 'r') as filehandle:
    for line in filehandle:
        qred = line
        qred.rstrip('\n')
        red.append(qred)
with open('questionsb.txt', 'r') as filehandle:
    for line in filehandle:
        qblue = line
        qblue.rstrip('\n')
        blue.append(qblue)


def choose():
    root.destroy()
    choosing = True
    while choosing:
        cred = input('For Red: ')
        cblue = input('For Blue: ')
        if cred and cblue:
            os.system('clear')
            with open('questionsr.txt', 'a') as filehandle:
                filehandle.write('\n'+cred)
            with open('questionsb.txt', 'a') as filehandle:
                filehandle.write('\n'+cblue)
            choosing = False
        else:
            print('Enter a proper question!')


def choosenr():
    red.remove(rred)
    blue.remove(rblue)
    precentnum = random.randint(1,100)
    if precentnum < 50:
        print(f'{precentnum}%\nRed Loses')
    elif precentnum > 50:
        print(f'{precentnum}%\nRed Wins')
    else:
        print('Red A Blue Are Equal')
    root.destroy()


def choosenb():
    red.remove(rred)
    blue.remove(rblue)
    precentnum = random.randint(1,100)
    if precentnum < 50:
        print(f'{precentnum}%\nBlue Loses')
    elif precentnum > 50:
        print(f'{precentnum}%\nBlue Wins')
    else:
        print('Red A Blue Are Equal')
    root.destroy()


while True:
    if len(red) == 0:
        print('Out Of Questions')
        break
    else:
        num = random.randint(0, len(red)-1)
        rred = red[num]
        rblue = blue[num]
        input('Press Enter To Start ')
        root = Tk()
        root.title('WOULD YOU RATHER')
        root.configure(bg='gray')
        Label(root, text='Would You Rather?', bg='gray', font=("", 55)).pack()
        Button(root, text=rred, bg='red', command=choosenr, height=6, width=100, font=("", 25)).pack()
        Label(root, text='OR', bg='gray', font=("", 20)).pack(pady=15)
        Button(root, text=rblue, bg='blue', command=choosenb, height=6, width=100, font=("", 25)).pack()
        Button(root, text='Add Question', bg='lightgray', command=choose).pack(pady=20)

        root.overrideredirect(True)
        root.geometry("{0}x{1}+0+0".format(root.winfo_screenwidth(), root.winfo_screenheight()))
        root.focus_set()

        root.mainloop()
