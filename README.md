BASIC WOULD YOU RATHER
BY HAMSH LESTER
WRITEN IN PYTHON3



Contains 3 main files

Project.py is the main source of code.

questionsb are the blue button questions.
questionsr are the red button questions.

when adding questions directly to file, make sure the question is not to long 
and that the questions are on the same line.


Example 1. Correct Way

```
\\
questionsr.txt
1. Never Have to Drink Again    # Will be assigned to the red button as question 1.
2.
3.
4.
5.
\\
```

```
\\
questionsb.txt
1. Never Have to Eat Again      # Will be assigned to the blue button as question 1.
2.
3.
4.
5.
\\
```


Example 2. Wrong Way

```
\\
questionsr.txt
1. Never Have to Drink Again    # Will be assigned to the red button as question 1.
2.
3.
4.
5.
\\
```

```
\\
questionsb.txt
1.
2. Never Have to Eat Again      # Will be assigned to the blue button as question 2.
3.
4.
5.
\\
```


when adding questions dont leave a empty line bellow.


Example 1. Correct Way

```
\\
questionsr.txt
1. Q1
2. Q2
3. Q3
4. Q4
5. Q5
\\
```

```
\\
questionsb.txt
1. Q1
2. Q2
3. Q3
4. Q4
5. Q5
\\
```


Example 2. Wrong Way

```
\\
questionsr.txt
1. Q1
2. Q2
3. Q3
4. Q4
5. 
\\
```

```
\\
questionsb.txt
1. Q1
2. Q2
3. Q3
4. Q4
5. 
\\
```


Must have a Blue and a Red Question For Each Question


Example 1. Correct Way

```
\\
questionsr.txt
1. Can Fly
2.
3.
4.
5.
\\
```

```
\\
questionsb.txt
1. Can Run Fast
2.
3.
4.
5.
\\
```


Example 2. Wrong Way

```
\\
questionsr.txt
1. Can Fly
2.
3.
4.
5.
\\
```

```
\\
questionsb.txt
1. 
2.
3.
4.
5.
\\
```